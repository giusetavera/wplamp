# WP Lamp
WP Lamp is a Vagrant Box environment optimized for WordPress CMS development. It provides a LAMP stack with **phpMyAdmin** and **PHP 7.4** already up and running. Just download the latest WordPress version and launch the setup wizard.

![](img/gitlab-preview.png)

## Getting Started
### Dependencies
 - **VirtualBox:** https://www.virtualbox.org/wiki/Downloads
 - **Vagrant:** https://www.vagrantup.com/downloads
 - **WP Lamp:** https://app.vagrantup.com/giusetavera/boxes/wplamp

### How it works
The working directory is 

    /var/www/html/..

### Template for Vagrantfile
Just an easy template ready to use

    Vagrant.configure("2") do |config|
        config.vm.box = "giusetavera/wplamp"
        config.vm.hostname = "your.local.host"
        config.vm.network :private_network, ip: "<ip address>"
        config.vm.synced_folder "/path/to/local/folder/", "/var/www/html"
    end

## Credentials
Sudoer user and root MySQL user credentials are:

 - **user:** root 
 - **password:** root

## Environment
 - **OS:** Linux Ubuntu 20.04
 - **Web Server:** Apache 2
 - **PHP:** 7.4, 8.1
 - **Database:** MySQL 5.7
 - **phpMyAdmin:** siteurl.est/phpmyadmin

## Bash scripts
Here is a short documentation of all the functions and trick provided by WP Lamp Vagrant box: switch PHP version, WP CLI already up and running and some other Bash scripts easy to use. Scripts are located in: 

    /usr/local/bin/

Complete list of provided scripts:
 - **wp-permissions:** reset WordPress permissions to default
 - **use-php:** switch between PHP versions 7.4 and 8.0
 - **dump:** backup your database quickly
 - **accio-node:** install last version of NodeJS
 - **accio-composer:** install Composer

### PHP 8.0
Default PHP version is 7.4 but 8.0 has already been installed (from box version 1.1). You wanna try? Just launch command **use-php** *followed by your desired version*. For example:

    use-php 8.0 # Switch and run PHP 8.0
    use-php 7.4 # Just restore the default version of the box

**Version 2.0 does not support previous version of PHP, just 8.1.x, because PHP 7.4 will be officialy deprecated since November 2022. So use-php script is not included in newer version of the box.**

### Reset WordPress root permissions
Version 1.1 provides a script to reset WordPress root folder permissions in a snap. Just lunch command **wp-permissions** followed by the folder (or the path) of your WordPress installation.

    wp-permissions /path/to/your/wp/folder
    wp-permissions html/

### Dump
This is kind of a shortcut for mysqldump command. It will export a backup copy of your database with current date. You will get a SQL file name like: **database--AAAAMMDD.sql**

    dump <name of your database>

### WP CLI
You can also find WP CLI already installed.

    /usr/local/bin/wp

### Accio-script
Fire the **accio-scripts** anywhere you want and install Composer or NodeJS quickly.

    accio-composer
    accio-node

## Author

This Vagrant box is created and maintened by Giuseppe Tavera.
Feel free to contact me for any issue: https://www.giuseppetavera.it

## Version History
* 2.0
	* Add: Accio script to install NodeJS
    * Add: Accio script to install Composer
    * Remove: PHP 7.4
    * Fix: error 503 in phpMyAdmin 
    * Minor fixes
* 1.4
	* Add: Dump bash script 
    * Minor fixes
* 1.3
    * Add: WP-CLI
* 1.2
    * Minor fixes
* 1.1
	* Add: WP Permission bash script 
    * Minor fixes
* 1.0
    * Initial Release

## License
Do whatever you want. Just remember to credit :-)
